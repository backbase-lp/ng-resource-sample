'use strict';

var gulp = require('gulp');
var path = require('path');
var g = require('gulp-load-plugins')();
var del = require('del');
var runSequence = require('run-sequence');
var widget = require('./package.json');

var paths = {
	src : ['./scripts'],
	docs : './docs',
	target : './target',
	templates : ['./templates'],
	styles : ['./styles'],
	test : ['./test']
};


/*----------------------------------------------------------------*/
/* Webpack Configuration
/*----------------------------------------------------------------*/
var config = {
	output: {
		filename: 'index.js',//path.join(widget.name,widget.main),
		libraryTarget: 'amd'
	},
	externals: [{
			angular: true,
			jquery: true,
		},
		// Every non-relative module is external
		/^[a-z\-0-9]+$/,
		// Every module prefixed with "lp/" becomes external
		function(context, request, callback) {
			// "lp/<module>"
			if(/^lp/.test(request)) {
				return callback(null, request);
			}
			callback();
		}
	]
};

/*----------------------------------------------------------------*/
/* Test/Lint
/*----------------------------------------------------------------*/
gulp.task('lint', function() {

});
gulp.task('test', function() {

});

/*----------------------------------------------------------------*/
/* Docs
/*----------------------------------------------------------------*/
gulp.task('clean:docs', function (done) {
	del([paths.docs], done);
});

gulp.task('docs', ['clean:docs'], function() {
	return gulp.src(paths.src + '/**/*.js')
		.pipe(g.jsdoc(paths.docs));
});

/*----------------------------------------------------------------*/
/* Scripts
/*----------------------------------------------------------------*/
gulp.task('build:scripts', [], function() {

	return gulp.src( widget.main )
		.pipe(g.webpack(config))
		.pipe(g.sourcemaps.init())
		.pipe(g.ngAnnotate())
		.pipe(g.uglify({
			mangle: { except: ['require'] }
		}))
		.pipe(g.sourcemaps.write('./', {
			addComment: false
		}))
		.pipe(gulp.dest(paths.target));

});

/*----------------------------------------------------------------*/
/* Styles
/*----------------------------------------------------------------*/
gulp.task('build:styles', [], function() {

});
/*----------------------------------------------------------------*/
/* Html
/*----------------------------------------------------------------*/
gulp.task('build:html', [], function() {

});
/*----------------------------------------------------------------*/
/* Build Scripts
/*----------------------------------------------------------------*/
gulp.task('clean:target', function (done) {
	del([paths.target], done);
});

gulp.task('build', function(done) {
	runSequence('clean:target', ['build:scripts', 'build:styles'], 'build:html',  done);
});

/*----------------------------------------------------------------*/
/* Default task
/*----------------------------------------------------------------*/
gulp.task('default', ['build','docs','lint', 'test' ], function () {

});

/*----------------------------------------------------------------*/
/* Watcher
/*----------------------------------------------------------------*/
gulp.task('watch', ['build'], function () {
	gulp.watch([ paths.src + '/**/*'], ['build', 'docs', 'lint', 'test']);
});

