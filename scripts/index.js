
define( function (require, exports, module) {

	'use strict';

	module.name = 'ng-resource-sample';
	/**
	 * Dependencies
	 */
	var main     = require('lp/main');
	var core     = require('lp/modules/core');

	var deps = [
		core.name
	];

	// @ngInject
	var run = function(widget, utils) {
		// NOTIFY


	};


	module.exports = main.createModule(module.name , deps).run( run );
});
